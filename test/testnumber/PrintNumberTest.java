package testnumber;

import org.junit.Test;
import static org.junit.Assert.*;

public class PrintNumberTest {

    public PrintNumberTest() {
    }

    /**
     * Test of display method, of class PrintNumber.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
        int i = 1;
        PrintNumber instance = new PrintNumber();
        String expResult = "1";
        String result = instance.display(i);
        assertEquals(expResult, result);
    }

}
