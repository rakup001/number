
package testnumber;

public class Number {

    
    public static void main(String[] args) {
        PrintNumber number = new PrintNumber(); // object created
        int n = 20;
        //loop 
        for (int i = n; i >= 1; i--) {
            String result = number.display(i);// calling function
            System.out.println(result); //printing result
        }
    }

}
