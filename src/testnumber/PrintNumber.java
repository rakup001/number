package testnumber;

public class PrintNumber {

    //method to display number with the symbol
    public String display(int i) {
        String result;
        if (i % 6 == 0) {
            if (i % 4 == 0) {
                result = i + "彡┻━┻ (ノಠ益ಠ)ノ";
                return result;
            } else {
                result = i + "彡┻━┻ノ";
                return result;
            }
        } else if (i % 4 == 0) {
            result = i + "(ノಠ益ಠ)ノ";
            return result;

        } else {
            result = Integer.toString(i); //converting integer to string
            return result;

        }

    }
}
